1. Disclaimer

TIBCO EMS and all related products are copyright and/or trademarks of TIBCO Software Inc - 3303 Hillview Avenue Palo Alto, CA 94304 USA.

WindowsTM, Sysinternals are copyright and trademark of Microsoft Corporation

All other product and company names and marks mentioned in this document are the property of their respective owners and are mentioned for identification purposes only.

THIS SOFTWARE IS PROVIDED “AS IS” WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESS OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT.
THIS DOCUMENT COULD INCLUDE TECHNICAL INACCURACIES OR TYPOGRAPHICAL ERRORS. CHANGES ARE PERIODICALLY ADDED TO THE INFORMATION HEREIN; THESE CHANGES WILL BE INCORPORATED IN NEW EDITIONS OF THIS DOCUMENT.

KAVAMALA LTD. MAY MAKE IMPROVEMENTS AND/OR CHANGES IN THE PRODUCT(S) AND/OR THE PROGRAM(S) DESCRIBED IN THIS DOCUMENT AT ANY TIME. THE CONTENTS OF THIS DOCUMENT MAY BE MODIFIED AND/OR QUALIFIED, DIRECTLY OR INDIRECTLY, BY OTHER DOCUMENTATION WHICH ACCOMPANIES THIS SOFTWARE, INCLUDING BUT NOT LIMITED TO ANY RELEASE NOTES AND "READ ME" FILES.


2. How to use this script

Clone/Download the repository, then: 
- copy the file in your /etc/init.d folder (you probably need root permissions to copy the file there, even if it's not strictly required for the script to be run as root).
- edit the file to set appropriate value for your TIBCO_HOME, EMS version, etc., pay attention to use the right executable: if you want to run the 64bit version you need to set prog and processname to tibemsd64, for the 32bit version it's just tibemsd.
- set the instance name and rename the file is you deem it appropriate
- register the service with: chkconfig -add ems-7222

Congratulations, your ems is now registered as service!
You can now use the standard "service" command to interact with it.

Examples:
service ems-7222 start
service ems-7222 stop
service ems-7222 status


3. Contacts & Info

For feedbacks and suggestions contact:
Daniele Galluccio <daniele.galluccio@kavamala.com>

Don't forget to check out EMS Butler, the ultimate EMS deployment tool.
EMS Butler is also included in Kavamala's flagship product: Release Butler.